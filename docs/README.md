---
home: true

heroImage: /hero.png

actionText: Find out more!
actionLink: /installation

features:
- title: Many-to-many relationship support
  details: Ability to keep track of changes in pivot tables, when dealing with BelongsToMany and MorphToMany relationships.

- title: Event source style approach
  details: Complete snapshots of Recordable models are kept when creating, modifying or retrieving records.

- title: Restore Recordable states
  details: Extract the state of a Recordable model from a given point in time.

- title: Signed Ledger records
  details: Store data signatures for an effortless data integrity check.

- title: Recording contexts
  details: Know in which context a Ledger was created.

- title: Huge customisation support
  details: Create your own custom drivers, resolvers and more!

footer: MIT Licensed | Copyright (C) 2018-2023 Quetzy Garcia
---
